import { user } from "./user.js";

console.dir(user);

console.log(
  `hello ${user.firstname}, i know your password :-) ${user.password}`
);

document.querySelector("#firstname").textContent = user.firstname;

// on stocke toutes les opérations de compte dans un array[]
const operationsCompte = [
  ["+", "salaire", 1520],
  ["-", "achat PS4", 499.99],
  ["-", "achat TV", 599],
];

function calcul() {
  // on déclare nos variables
  let totalCredit = 0;
  let detailsCredit = ""; //va recueillir l'ensemble des opérations Crédit [libellé et montant]
  let totalDebit = 0;
  let detailsDebit = ""; //va recueillir l'ensemble des opérations Dédit [libellé, montant & pourcentage]
  let solde = 0;
  let operator = "";
  let devise = "€";
  //doc forEach https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/forEach
  operationsCompte.forEach(function (data) {
    // console.log(data[0]);
    if (data[0] === "+") {
      totalCredit += data[2];
      detailsCredit += `<li><span class="intitule">${data[1]}</span><span class="montant txt-color-gazoil">${data[2]} ${devise}</span></li>`;
    } else {
      totalDebit += data[2];
      detailsDebit += `<li><span class="intitule">${data[1]}</span><span class="montant txt-color-red">${data[2]} ${devise}</span><span class="percent txt-color-red">%</span></li>`;
    }
    solde = totalCredit - totalDebit;
  });

  console.log("description Credit", detailsCredit);
  console.log("description Debit", detailsDebit);

  console.log("totalCredit", totalCredit);
  console.log("totalDebit", totalDebit);
  console.log("solde", solde);

  if (solde >= 0) {
    operator = "+";
  } else {
    document.getElementById("total").style.color = "red"; // on affiche le solde en couleur rouge
  }
  // on affiche les résultats dans le DOM
  document.getElementById(
    "total"
  ).textContent = `${operator} ${solde} ${devise}`;
  document.getElementById(
    "totalCredit"
  ).textContent = `${totalCredit} ${devise}`;
  document.getElementById("totalDebit").textContent = `${totalDebit} ${devise}`;
  document.querySelector("#detailsDebit").innerHTML = detailsDebit;
  document.querySelector("#detailsCredit").innerHTML = detailsCredit;
}
// on execute la function
calcul();

// send form, add operation
const formulaire = document.getElementById("ajoutOperation");
formulaire.addEventListener("submit", function (e) {
  e.preventDefault();
  // on récupère les valeurs des champs du formulaire
  const operation = document.getElementById("operation").value;
  const libelle = document.getElementById("intitule").value;
  const montant = document.getElementById("montant").value;
  // on stocke ces valeurs dans un array[]
  const addOperation = [operation, libelle, Number(montant)];
  // on ajoute cet array dans notre array global operationsCompte
  operationsCompte.push(addOperation);
  console.table(operationsCompte);
  // on execute la fonction pour actualiser
  calcul();
  // on reset le formulaire
  formulaire.reset();
});
